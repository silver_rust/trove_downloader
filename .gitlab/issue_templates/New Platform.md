# New Platform

## What is the code for the platform?
You can find a list of the platforms here: https://forge.rust-lang.org/release/platform-support.html.  
For example 64bit Windows is ``x86_64-pc-windows-gnu`` or ``x86_64-pc-windows-msvc``

## Does it require an extension?
For example ``.exe`` on Windows.