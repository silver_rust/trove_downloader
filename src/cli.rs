extern crate structopt;
pub use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(about, author)]
pub enum Options {
    /// Update the program. its basic in that it downloads a fresh copy from the repo, dosnt check version or anything yet
    Update {
        /// Pass true here to upgrade unattended
        #[structopt(short, long)]
        unattended: bool,
    },
    /// How the program runs normally
    Run {
        /// Session key
        /// Optional after the first run.
        #[structopt(short, long)]
        key: Option<String>,

        /// Save Location
        /// Defaults to same folder as executable if left blank.
        /// Optional after the first run.
        #[structopt(short, long)]
        location: Option<String>,

        /// Platform
        /// Possible Values: all, windows, mac, linux, asmjs, unitywasm.
        /// Defaults to windows if left blank.
        /// Optional after the first run.
        #[structopt(short, long)]
        platform: Option<String>,

        /**
        Folders
        Decides the folder layout
        Original: Puts files into folders based off of what humble themselves suggest,
            ./windows/Frozenbyte/trinegame_v1234.zip

        Game: Puts files in a folder of the games name such as
            ./windows/Trine/trinegame_v1234.zip

        Reverse: Puts files in a game folder first then sunbdevides into platform.
            ./Trine/windows/trinegame_v1234.zip

        Defaults to Game.

        Also has two modifiers: no-whitespace,lowercase
        These change the download location to having no spaces and using lowercase characters only.
        You need to append them onto a custom format "platform,name,no-whitespace" or "platform,name,lowercase" or "platform,name,no-whitespace,lowercase"
        */
        #[structopt(short, long)]
        folders: Option<String>,
    },
    /// Upgrade the config files
    Upgrade {
        /// v1 to v2
        /// Will overwrite v2 files if they already exist
        #[structopt(long)]
        v1: bool,
    },
}
