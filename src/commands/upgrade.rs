use super::files::{v1, v2};
use std::collections::BTreeMap;

pub fn v1_v2() {
    // config
    // get original
    let config_old: v1::Config = v1::get_config();
    println!("config.json: Got v1");

    // convert
    let config_new: v2::Config = v1_v2_config(config_old);
    println!("config.json: Converted v1 to v2");

    // save
    v2::save_config(&config_new);
    println!("config.json: Saved new v2");

    // progress
    // get original
    let progress_old = v1::get_progress();
    println!("progress.json: Got v1");

    // convert
    let progress_new = v1_v2_progress(progress_old);
    println!("progress.json: Converted v1 to v2");

    // save
    v2::save_progress(&progress_new);
    println!("progress.json: Saved new v2");
}

fn v1_v2_config(config_old: v1::Config) -> v2::Config {
    let api_new = config_old
        .api
        .replace("_simpleauth_sess=\"", "")
        .replace("\"", "");

    let folders_new = if config_old.folders == "" {
        "Game".to_string()
    } else {
        config_old.folders
    };

    v2::Config {
        api: api_new,
        platform: config_old.platform,
        location: config_old.location,
        folders: folders_new,
    }
}

fn v1_v2_progress(progress_old: BTreeMap<String, v1::Progress>) -> BTreeMap<String, v2::Progress> {
    let mut progress_new: BTreeMap<String, v2::Progress> = BTreeMap::new();

    // loop through progress_old
    for (name, item) in progress_old.iter() {
        let mut downloads: BTreeMap<String, v2::ProgressPlatform> = BTreeMap::new();

        // I hate this, but cannot seem to access it otherwise
        if &item.asmjs != "" && &item.asmjs != "none" {
            downloads.insert(
                "asmjs".to_string(),
                v2::ProgressPlatform {
                    platform: "asmjs".to_string(),
                    hash: item.asmjs.to_string(),
                    location: "".to_string(),
                    size: 0,
                    downloaded: item.downloaded.to_string(),
                    downloaded_local: item.downloaded_local.to_string(),
                },
            );
        }
        if &item.linux != "" && &item.linux != "none" {
            downloads.insert(
                "linux".to_string(),
                v2::ProgressPlatform {
                    platform: "linux".to_string(),
                    hash: item.linux.to_string(),
                    location: "".to_string(),
                    size: 0,
                    downloaded: item.downloaded.to_string(),
                    downloaded_local: item.downloaded_local.to_string(),
                },
            );
        }
        if &item.mac != "" && &item.mac != "none" {
            downloads.insert(
                "mac".to_string(),
                v2::ProgressPlatform {
                    platform: "mac".to_string(),
                    hash: item.mac.to_string(),
                    location: "".to_string(),
                    size: 0,
                    downloaded: item.downloaded.to_string(),
                    downloaded_local: item.downloaded_local.to_string(),
                },
            );
        }
        if &item.unitywasm != "" && &item.unitywasm != "none" {
            downloads.insert(
                "unitywasm".to_string(),
                v2::ProgressPlatform {
                    platform: "unitywasm".to_string(),
                    hash: item.unitywasm.to_string(),
                    location: "".to_string(),
                    size: 0,
                    downloaded: item.downloaded.to_string(),
                    downloaded_local: item.downloaded_local.to_string(),
                },
            );
        }
        if &item.windows != "" && &item.windows != "none" {
            downloads.insert(
                "windows".to_string(),
                v2::ProgressPlatform {
                    platform: "windows".to_string(),
                    hash: item.windows.to_string(),
                    location: "".to_string(),
                    size: 0,
                    downloaded: item.downloaded.to_string(),
                    downloaded_local: item.downloaded_local.to_string(),
                },
            );
        }

        let image = if &item.image == "none" {
            ""
        } else {
            &item.image
        };

        progress_new.insert(
            name.to_string(),
            v2::Progress {
                name: item.name.to_string(),
                image: image.to_string(),
                size: 0,
                downloads,
                date_added: "".to_string(),
                last_seen: "".to_string(),
            },
        );
    }

    progress_new
}
