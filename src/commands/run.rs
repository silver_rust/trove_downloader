extern crate chrono;
extern crate reqwest;

use super::files::v2::{
    get_config, get_progress, get_trove_data, save_progress, Config, Download, DownloadUrl,
    ItemData, Progress, ProgressPlatform,
};

use chrono::{prelude::*, Local, Utc};
use std::{collections::BTreeMap, fs, fs::File, io, io::BufWriter};

pub fn run(
    key: Option<String>,
    location: Option<String>,
    platform: Option<String>,
    folders: Option<String>,
) {
    // get config
    let config = get_config(key, location, platform, folders);

    // get current trove list
    let trove = get_trove_data(&config.api);

    // get array of platforms
    let platforms = get_platforms(&config.platform);

    process_download(&config, &trove, &platforms);
}

// my main function for downloading files
fn get_file(url: &str, dir: &str, file: &str) {
    let mut resp = reqwest::blocking::get(url).expect("request failed");

    // breaks it off if anythign goes wonky
    if !resp.status().is_success() {
        println!("Error downloading: {} . Got {}", url, resp.status());
        panic!();
    }

    fs::create_dir_all(dir).expect("Something went wrong creating the directory");
    let out = File::create(file).expect("failed to create file");
    let mut stream = BufWriter::new(out);
    io::copy(&mut resp, &mut stream).expect("failed to copy content");
}

fn get_platforms(selected_platform: &str) -> Vec<&str> {
    let platforms_supported: Vec<&str> = vec!["asmjs", "linux", "mac", "unitywasm", "windows"];

    if selected_platform == "all" {
        return platforms_supported;
    }

    if selected_platform.contains(',') {
        let mut tmp: Vec<&str> = Vec::new();

        let platforms = selected_platform.split(',');

        for s in platforms {
            if platforms_supported.contains(&s) {
                tmp.push(&s)
            }
        }
        return tmp;
    }

    if platforms_supported.contains(&selected_platform) {
        vec![selected_platform]
    } else {
        platforms_supported
    }
}

fn get_download_links(auth: &str, machine_name: &str, filename: &str) -> String {
    let params = [("machine_name", machine_name), ("filename", filename)];
    let cookie = "_simpleauth_sess=\"".to_string() + auth + "\"";
    let client = reqwest::blocking::Client::new();

    let download_url: DownloadUrl = client
        .post("https://www.humblebundle.com/api/v1/user/download/sign")
        .form(&params)
        .header("cookie", &cookie)
        .send()
        .expect("\n\nSomething went wrong getting the download link\n\n")
        .json::<DownloadUrl>()
        .expect(
            "\n\nPlease verify your API key and make sure that your subscription is active\n\n",
        );

    download_url.signed_url
}

fn remove_slashes(s: &str) -> (&str, &str) {
    s.split_at(
        s.len()
            - s.chars()
                .rev()
                .position(|x| x == '/')
                .unwrap_or_else(|| s.len()),
    )
}

fn get_directory(config: &Config, platform: &str, name: &str, offset: &str) -> String {
    let name_clean: &str = &*name
        .replace(|c: char| !(c.is_alphanumeric() || c == ' '), "")
        .replace("  ", " ")
        .replace("   ", " ");

    // defaults to Game
    let custom = match &config.folders as &str {
        "Original" => "platform,offset",
        "Game" => "platform,name",
        "Reverse" => "name,platform",
        _ => &config.folders,
    };

    let mut directory_tmp: Vec<&str> = Vec::new();
    for s in custom.split(',') {
        let sub_item = match s {
            "platform" => platform,
            "offset" => offset,
            "name" => name_clean,
            "name_raw" => name,
            // catch all non valid options
            _ => "",
        };
        if sub_item != "" {
            directory_tmp.push(sub_item);
        }
    }
    // just in case its only modifiers, default to Game
    if directory_tmp.is_empty() {
        directory_tmp.push(platform);
        directory_tmp.push(name_clean);
    }

    let location_base = if config.location.contains('~') {
        shellexpand::tilde(&config.location).to_string()
    } else {
        config.location.to_string()
    };

    let mut directory: String = directory_tmp.join("/");

    // final processing
    // whitespace
    if config.folders.contains("no-whitespace") {
        directory.retain(|c| !c.is_whitespace());
    }

    //lowercase
    if config.folders.contains("lowercase") {
        directory.make_ascii_lowercase()
    }

    location_base + "/" + directory.as_ref() + "/"
}

fn process_download(config: &Config, trove: &[ItemData], platforms: &[&str]) {
    // get the progress list and keep it in memory
    let mut progress = get_progress();

    let mut n: i32 = 0;
    // loop through all the items
    for item in trove.iter() {
        // get
        let name = &item.human_name;
        let image = &item.image;
        let date_added = epoch_to_iso(item.date_added);
        let downloads: &BTreeMap<String, Download> = &item.downloads;

        // check if ther eis already an entry in the progress
        if !progress.contains_key(name) {
            progress.insert(
                name.to_string(),
                Progress {
                    name: name.to_string(),
                    image: image.to_string(),
                    size: 0,
                    downloads: BTreeMap::new(),
                    date_added,
                    last_seen: now_to_iso(),
                },
            );
        } else {
            // if it already exists
            let progress_existing = progress.get(name).unwrap().clone();

            let date_added = if progress_existing.date_added == "" {
                epoch_to_iso(item.date_added)
            } else {
                progress_existing.date_added
            };

            progress.insert(
                name.to_string(),
                Progress {
                    name: name.to_string(),
                    image: image.to_string(),
                    size: progress_existing.size,
                    downloads: progress_existing.downloads,
                    date_added,
                    last_seen: now_to_iso(),
                },
            );
        };

        // for each platform in platforms
        for &platform in platforms.iter() {
            // check if there is a download for this platform
            // if there isnt it moveds onto the next platform
            let download_data = match downloads.get(platform) {
                Some(x) => x,
                None => continue,
            };

            let tmp_process = progress.clone();
            let item_progress = tmp_process
                .get(name)
                .unwrap_or_else(|| panic!("could not find {} in progress", name));

            if let Some(x) = item_progress.downloads.get(platform) {
                if x.hash == download_data.md5 {
                    continue;
                };
            };

            // if there is
            // get the download link from Humble Bundle's API
            let download_url = get_download_links(
                &config.api,
                &download_data.machine_name,
                &download_data.url.web,
            );

            // some files are stored in folders, deal with this
            let (offset, file_name) = remove_slashes(&download_data.url.web);

            // get download location
            let directory = get_directory(&config, platform, &name, offset);
            let file = directory.to_owned() + file_name;

            // then download
            // downloading the actual file and saving it to the right location
            println!(
                "Downloading {} file {}/{} {}",
                platform,
                n + 1,
                trove.len(),
                &file
            );
            get_file(&download_url, &directory, &file);

            //// update progress

            // update size stored
            let new_size = item_progress.size + download_data.file_size;

            let mut new_downloads = item_progress.downloads.clone();
            // update the specific platform
            new_downloads.insert(
                platform.to_string(),
                ProgressPlatform {
                    downloaded: now_to_iso(),
                    downloaded_local: format!("{:?}", Local::now()),
                    platform: platform.to_string(),
                    hash: download_data.md5.to_string(),
                    location: file.to_string(),
                    size: download_data.file_size,
                },
            );

            // update the overall progress progress
            progress.insert(
                name.to_string(),
                Progress {
                    name: (&item_progress.name).parse().unwrap(),
                    image: (&item_progress.image).parse().unwrap(),
                    size: new_size,
                    date_added: (&item_progress.date_added).parse().unwrap(),
                    last_seen: now_to_iso(),
                    downloads: new_downloads,
                },
            );

            // flush to disk
            // ye I know its blocking but I want to save progress for each download after its complete
            save_progress(&progress);
        }
        n += 1;
    }
}

fn epoch_to_iso(timestamp: i64) -> String {
    Utc.timestamp(timestamp, 0)
        .to_rfc3339_opts(SecondsFormat::Secs, true)
}

fn now_to_iso() -> String {
    Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true)
}
