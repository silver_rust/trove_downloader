pub mod v2 {
    extern crate reqwest;

    use serde::{Deserialize, Serialize};
    use std::{collections::BTreeMap, fs, fs::File, io::BufReader};

    // this is the config file
    #[derive(Serialize, Deserialize, Debug)]
    pub struct Config {
        pub api: String,
        pub platform: String,
        pub location: String,
        pub folders: String,
    }

    // this gets and sets the config
    pub fn get_config(
        api: Option<String>,
        location: Option<String>,
        platform: Option<String>,
        folders: Option<String>,
    ) -> Config {
        let config_path = "config/config_v2.json";

        // if it already exists use those valuies
        let config = if fs::metadata(&config_path).is_ok() {
            println!("Using arguments from config file");
            let file =
                File::open(&config_path).expect("Something went wrong opening the config file");
            let reader = BufReader::new(file);
            let config_file: Config = serde_json::from_reader(reader)
                .expect("Something went wrong reading the config file");

            Config {
                // if these are specified switch to the new ones, else use the ones on file

                // if new parameters exist use those, else use the previous versions
                api: api.unwrap_or(config_file.api),
                platform: platform.unwrap_or(config_file.platform),
                location: location.unwrap_or(config_file.location),
                folders: folders.unwrap_or(config_file.folders),
            }
        } else {
            // if it dosent use the passed config values
            println!("Creating new config file");

            Config {
                // cant do anything without the session key
                api: api.expect("session key is not set"),

                // set defaults for these
                platform: platform.unwrap_or_else(|| "windows".to_string()),
                location: location.unwrap_or_else(|| ".".to_string()),
                folders: folders.unwrap_or_else(|| "Game".to_string()),
            }
        };

        save_config(&config);
        config
    }

    pub fn save_config(config: &Config) {
        let config_path = "config/config_v2.json";
        fs::create_dir_all("config/").expect("Something went wrong creating the directory");

        let file = File::create(&config_path).expect("Unable to create file");
        serde_json::to_writer_pretty(file, config).expect("Unable to write file");
    }

    // these are for the trove.json
    #[derive(Serialize, Deserialize, Debug)]
    pub struct ItemData {
        pub downloads: BTreeMap<String, Download>,
        #[serde(rename = "human-name")]
        pub human_name: String,
        pub image: String,
        #[serde(rename = "date-added")]
        pub date_added: i64,
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Download {
        pub url: Url,
        pub md5: String,
        pub machine_name: String,
        pub file_size: i64,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "camelCase")]
    pub struct Url {
        pub web: String,
        //bittorrent: String,
    }

    pub fn get_trove_data(auth: &str) -> Vec<ItemData> {
        let mut new_json: Vec<ItemData> = Vec::new();

        let mut n: i32 = 0;
        let mut done = false;
        let url_base = "https://www.humblebundle.com/client/catalog?index=";
        let cookie = "_simpleauth_sess=\"".to_string() + auth + "\"";

        let client = reqwest::blocking::Client::new();
        while !done {
            let url: &str = &[url_base, &n.to_string()].concat();

            let json: Vec<ItemData> = client
                .get(url)
                .header("cookie", &cookie)
                .send()
                .expect("Something went wrong getting the trove data")
                .json::<Vec<ItemData>>()
                .expect("Something went wrong converting the trove data");

            if json.is_empty() {
                done = true;
            }

            for x in json {
                new_json.push(x);
            }
            n += 1;
        }
        println!("Trove has {} items in it", new_json.len());

        new_json.sort_by(|a, b| a.human_name.cmp(&b.human_name));

        let dir = "config/";
        let file = "config/trove.json";

        fs::create_dir_all(dir).expect("Something went wrong creating the directory");

        let file = File::create(file).expect("Unable to create file");
        serde_json::to_writer_pretty(file, &new_json).expect("Unable to write file");

        new_json
    }

    // these two handle getting and setting teh progress
    #[derive(Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct Progress {
        pub name: String,
        pub image: String,
        pub size: i64,
        pub date_added: String,
        pub last_seen: String,
        pub downloads: BTreeMap<String, ProgressPlatform>,
    }

    #[derive(Serialize, Deserialize, Debug, Clone)]
    #[serde(rename_all = "camelCase")]
    pub struct ProgressPlatform {
        pub platform: String,
        pub hash: String,
        pub location: String,
        pub size: i64,
        pub downloaded: String,
        pub downloaded_local: String,
    }

    pub fn get_progress() -> BTreeMap<String, Progress> {
        let path = "./config/progress_v2.json";
        // test getting teh file first
        let progress = if fs::metadata(&path).is_ok() {
            fs::File::open(&path).unwrap()
        } else {
            fs::write(&path, "{}").expect("Unable to write file");
            fs::File::open(&path).unwrap()
        };
        let reader = BufReader::new(progress);
        serde_json::from_reader(reader).expect("Something went wrong reading the config file")
    }

    pub fn save_progress(progress: &BTreeMap<String, Progress>) {
        let file = File::create("./config/progress_v2.json").expect("Unable to update Progress");
        serde_json::to_writer_pretty(file, progress).expect("Unable to write file");
    }

    // for getting teh signed link to download from
    #[derive(Serialize, Deserialize, Debug)]
    pub struct DownloadUrl {
        pub signed_url: String,
    }
}

pub mod v1 {
    use serde::{Deserialize, Serialize};
    use std::{collections::BTreeMap, fs, fs::File, io::BufReader, process};

    #[derive(Serialize, Deserialize, Debug)]
    pub struct Config {
        pub api: String,
        pub platform: String,
        pub location: String,
        // this is due to it not being in the config originally
        #[serde(default = "default_blank")]
        pub folders: String,
    }

    pub fn get_config() -> Config {
        let config_path = "config/config.json";

        // if it already exists use those valuies
        if fs::metadata(&config_path).is_ok() {
            let file = File::open(&config_path).expect("Opening Error");
            let reader = BufReader::new(file);
            let config_file: Config = serde_json::from_reader(reader).expect("Reading Error");
            config_file
        } else {
            println!("No v1 config file");
            process::exit(1);
        }
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct ItemData {
        downloads: Downloads,
        #[serde(rename = "human-name")]
        human_name: String,
        image: String,
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct DisplayItemData {
        downloads: Downloads,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "camelCase")]
    struct Downloads {
        windows: Option<Download>,
        mac: Option<Download>,
        linux: Option<Download>,
        asmjs: Option<Download>,
        unitywasm: Option<Download>,
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct Download {
        url: Url,
        md5: String,
        machine_name: String,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "camelCase")]
    struct Url {
        web: String,
    }

    #[derive(Serialize, Deserialize, Debug)]
    #[serde(rename_all = "camelCase")]
    pub struct Progress {
        pub name: String,
        #[serde(default = "default_progress")]
        pub downloaded: String,
        #[serde(default = "default_progress")]
        pub downloaded_local: String,
        #[serde(default = "default_progress")]
        pub image: String,
        #[serde(default = "default_progress")]
        pub windows: String,
        #[serde(default = "default_progress")]
        pub mac: String,
        #[serde(default = "default_progress")]
        pub linux: String,
        #[serde(default = "default_progress")]
        pub asmjs: String,
        #[serde(default = "default_progress")]
        pub unitywasm: String,
    }

    fn default_progress() -> String {
        "none".to_string()
    }

    fn default_blank() -> String {
        "".to_string()
    }

    pub fn get_progress() -> BTreeMap<String, Progress> {
        let config_path = "config/progress.json";

        // if it already exists use those valuies
        if fs::metadata(&config_path).is_ok() {
            let file = File::open(&config_path).expect("Opening Error");
            let reader = BufReader::new(file);
            let config_file: BTreeMap<String, Progress> =
                serde_json::from_reader(reader).expect("Reading Error");
            config_file
        } else {
            println!("No v1 progress file");
            process::exit(1);
        }
    }

    #[derive(Serialize, Deserialize, Debug)]
    struct DownloadUrl {
        signed_url: String,
    }
}
