use self_update::cargo_crate_version;

pub fn update(unattended: bool) -> Result<(), Box<dyn ::std::error::Error>> {
    let status = self_update::backends::gitlab::Update::configure()
        .repo_owner("silver_rust")
        .repo_name("trove_downloader")
        .bin_name("trove_downloader")
        .show_output(true)
        .no_confirm(unattended)
        .show_download_progress(true)
        .current_version(cargo_crate_version!())
        .build()?
        .update()?;
    println!("Update status: `{}`!", status.version());
    Ok(())
}
