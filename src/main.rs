mod cli;
mod commands;

use cli::{Options, StructOpt};
use commands::{run::run, update::update, upgrade::v1_v2};

fn main() {
    // decides what to do
    match Options::from_args() {
        Options::Update { unattended } => {
            // calls the update function
            update(unattended).ok();
        }
        // Sets up Parameters then runs
        Options::Run {
            key,
            location,
            platform,
            folders,
        } => {
            run(key, location, platform, folders);
        }
        Options::Upgrade { v1 } => {
            if v1 {
                v1_v2();
            }
        }
    }
}
