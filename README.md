# Trove Downloader

This program allows Humble Monthly subscribers to download all the games currently available in the Trove. 
Trove Downloader currently supports Windows and Linux. MacOS support is planned for the future.   
This program requires an active Humble Monthly subscription.

The Trove is a collection of 90+ DRM-Free games available to Humble Monthly subscribers. 
The collection include Humble original games alongside others, and are available to download only while subscribed to Humble Monthly. 
Games are added and removed every month, and there is no official way of tracking when games are removed from the Trove. This program aims to alleviate that.

*****
## Getting Started

An API key from Humble is required for this program to work. Instructions are provided in [API_KEY.md](API_KEY.md) on how to retrieve one.

To get started using Trove Downloader, either compile the program yourself, or download one of the binaries provided below.

The API key needs to be provided the first time the program is run. The key and all other parameters only need to be provided once. 
These parameters are saved and can be updated by giving a new flag on a future run. 
For example, running ``./trove_downloader run -p linux`` will change the platform parameter to Linux, regardless of what it was before.

### Pre Compiled

Pre-compiled binaries for Windows and Linux are available in [Releases](https://gitlab.com/silver_rust/trove_downloader/-/releases).

If you want it to be compiled to a specific type [open an issue](https://gitlab.com/silver_rust/trove_downloader/-/issues/new)

### Compiling

Make sure rust [is installed](https://www.rust-lang.org/tools/install) on your system.  
To compile trove_downloader:
 * Download the source files from this repo
 * Go the source directory from a terminal
 * Run ``cargo build --release``. 
 * The compiled program will be located under ``./target/release/`` in the source directory.

### Usage
To see the commands/flags please do: ``trove_downloader -h``

## Example Uses

### First run

This command will download all games for every platform to the location ``C:\Games\Trove`` with the Reverse folder order if run on Windows.
```
.\trove_downloader.exe run -k "API KEY HERE" -p "all" -l "C:\Games\Trove" -f Reverse
```

This command will download all games for every platform to the location ``~/games/trove`` with the Reverse folder order if run on Linux.
```
./trove_downloader run -k "API KEY HERE" -p "all" -l "~/games/trove" -f Reverse
```

### Subsequent runs

This command will download all games given the saved parameters on Windows.
``.\trove_downloader.exe run``

This command will download all games given the saved parameters on Linux.
``./trove_downloader run``

### Updating
This is to update trove_downloader to the latest version.

```bash
# Will ask you to verify if you want to update
./trove_downloader update

# Will update regardless/unattended
./trove_downloader update -u
```

### Upgrading
When I first released this tool I was new to rust and made a fuckton of mistakes.  
With v2 of the downloader I have fixed many of those, however config files had to go through a restructure.

Part of the v2 release is that I have an upgrade path for anyone who used the v1 of the downloader to convert the files to the v2 equivalent.

```bash
./trove_downloader upgrade --v1
```

*****

## FAQ

### The "Recently added games" were not downloaded.
These items are not yet added to the full trove list.  
[Expanded Answer](https://gitlab.com/silver_rust/trove_downloader/-/issues/22)


****
## To-Do List

* MacOS Support
* [add to do items here]

## License

This project is licensed under Blue Oak Model License version 1.0.0, as described in LICENSE.md.

## Contact

Discord: Silver#5563
Discord Server: https://discord.gg/4fQYyVb

Pull requests and ideas welcome.



This program is not affiliated nor endorsed by Humble Bundle, Inc.
